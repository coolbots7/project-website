var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
     res.render('api', {
          title: 'Express'
     });
});

router.get('/projects', function(req, res, next) {
     var json = require("../public/javascripts/projects.json");
     res.setHeader('Content-Type', 'application/json');
     res.end(JSON.stringify(json));
});

module.exports = router;
